﻿using LuminalTools.Core.Enumerations;

namespace LuminalTools.Web.Enumerations
{
	public sealed class MediaType : BaseStringEnum
	{
		public static readonly MediaType GIF = new MediaType("image/gif");
		public static readonly MediaType JPG = new MediaType("image/jpg");
		public static readonly MediaType PNG = new MediaType("image/png");

		private MediaType(string value) : base(value) { }
	}
}
