﻿using LuminalTools.Core.Utilities;
using LuminalTools.Web.Enumerations;
using System;
using System.IO;

namespace LuminalTools.Web.UserInterface
{
	public class DataUri
	{
		public string MediaType { get; private set; }
		public string Base64 { get; private set; }

		public DataUri(MediaType mediaType, Stream inputStream)
		{
			var inputBytes = StreamUtils.Parse(inputStream);

			Base64 = Convert.ToBase64String(inputBytes);
			MediaType = mediaType;
		}

		/// <summary>
		/// data:[mediatype][;base64],data
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return $"data:{MediaType};base64,{Base64}";
		}
	}
}
