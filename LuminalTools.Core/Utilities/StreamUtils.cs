﻿using System.IO;

namespace LuminalTools.Core.Utilities
{
	public static class StreamUtils
	{
		public static byte[] Parse(Stream input)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				input.CopyTo(ms);
				return ms.ToArray();
			}
		}
	}
}
