﻿using System;

namespace LuminalTools.Core.Enumerations
{
	/// <summary>
	/// Type-safe enum pattern: http://stackoverflow.com/a/424414
	/// </summary>
	public abstract class BaseStringEnum
	{
		public string Value { get; protected set; }

		protected BaseStringEnum(string value)
		{
			Value = value ?? throw new ArgumentNullException(nameof(value));
		}

		public override string ToString() => Value;

		public static implicit operator string(BaseStringEnum param)
		{
			if (param == null)
				throw new ArgumentNullException(nameof(param));

			return param.Value;
		}
	}
}
